﻿using System.Security.Claims;

namespace inventory
{
    public class AppUser : ClaimsPrincipal
    {
        public AppUser(ClaimsPrincipal principal)
            : base(principal)
        {
        }

        public string Name
        {
            get
            {
                return this.FindFirst(ClaimTypes.Name).Value;
            }
        }
        public string UserId
        {
            get
            {
                return this.FindFirst("USER_ID").Value;
            }
        }
        public string KodeCabang
        {
            get
            {
                return this.FindFirst("KD_CABANG").Value;
            }
        }
        public string KodeTerminal
        {
            get
            {
                return this.FindFirst("KD_TERMINAL").Value;
            }
        }
        public string KodeCabangOrig
        {
            get
            {
                return this.FindFirst("KD_CABANG_ORIG").Value;
            }
        }
        public string KodeTerminalOrig
        {
            get
            {
                return this.FindFirst("KD_TERMINAL_ORIG").Value;
            }
        }
        public string Role
        {
            get
            {
                return this.FindFirst("USER_ROLE").Value;
            }
        }
        public string RoleName
        {
            get
            {
                return this.FindFirst("USER_ROLE_NAME").Value;
            }
        }
        public string NamaCabang
        {
            get
            {
                return this.FindFirst("NAMA_CABANG").Value;
            }
        }
        public string NamaTerminal
        {
            get
            {
                return this.FindFirst("NAMA_TERMINAL").Value;
            }
        }
    }
}