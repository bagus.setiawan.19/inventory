﻿using Dapper;
using inventory.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace inventory.DAL
{
    public class HelperDAL
    {
        public IEnumerable<Select2Model> getSelect2ParentMenu()
        {
            IEnumerable<Select2Model> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"select id kode,nameoption nama from (
                                   select 0 id,'Root' nameoption from dual
                                   union all
                                   select id,nameoption from app_menu where isparent = 'true')
                                   order by 1";
                    listData = connection.Query<Select2Model>(sql);
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }
    }
}