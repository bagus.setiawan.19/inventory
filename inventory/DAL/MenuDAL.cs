﻿using Dapper;
using inventory.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace inventory.DAL
{
    public class MenuDAL
    {
        // Menu Navigation
        public List<APP_MENU> GetMenu(int role_id)
        {
            string query = "";
            List<APP_MENU> AllData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    if (role_id == 1)
                    {
                        query = "SELECT ID,NAMEOPTION,CONTROLLER,ACTION,IMAGECLASS,STATUS,ISPARENT,PARENTID,ORDER_BY FROM APP_MENU ORDER BY PARENTID, ID";
                    }
                    else
                    {
                        // ambil menu berdasarkan role
                        //query = "SELECT ID,NAMEOPTION,CONTROLLER,ACTION,IMAGECLASS,STATUS,ISPARENT,PARENTID,ORDER_BY FROM APP_MENU WHERE ORDER BY PARENTID, ID";
                        query = null;
                    }

                    AllData = connection.Query<APP_MENU>(query).ToList();
                }
                catch (Exception)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public IEnumerable<APP_MENU> GetDataMenu()
        {
            IEnumerable<APP_MENU> AllData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string query = "SELECT ID,NAMEOPTION,CONTROLLER,ACTION,IMAGECLASS,STATUS,ISPARENT,PARENTID,ORDER_BY FROM APP_MENU ORDER BY PARENTID, ID";
                    AllData = connection.Query<APP_MENU>(query).ToList();
                }
                catch (Exception)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public dynamic Add(APP_MENU data)
        {
            dynamic result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var Param = new DynamicParameters();
                    Param.Add("XNAMEOPTION", data.NAMEOPTION);
                    Param.Add("XCONTROLLER", data.CONTROLLER);
                    Param.Add("XACTION", data.ACTION);
                    Param.Add("XIMAGECLASS", data.IMAGECLASS);
                    Param.Add("XSTATUS", data.STATUS);
                    Param.Add("XISPARENT", data.ISPARENT);
                    Param.Add("XPARENTID", data.PARENTID);
                    Param.Add("XSTS", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    Param.Add("XMSG", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("INS_MENU", Param, null, null, commandType: CommandType.StoredProcedure);

                    result = new
                    {
                        sts = Param.Get<string>("XSTS"),
                        msg = Param.Get<string>("XMSG")
                    };
                }
                catch (Exception e)
                {
                    result = new
                    {
                        sts = "E",
                        msg = e.Message.ToString()
                    };
                }
            }
            return result;
        }

        public dynamic Del(APP_MENU data)
        {
            dynamic result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var Param = new DynamicParameters();
                    Param.Add("XID", data.ID);
                    Param.Add("XSTS", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    Param.Add("XMSG", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("DEL_MENU", Param, null, null, commandType: CommandType.StoredProcedure);

                    result = new
                    {
                        sts = Param.Get<string>("XSTS"),
                        msg = Param.Get<string>("XMSG")
                    };
                }
                catch (Exception e)
                {
                    result = new
                    {
                        sts = "E",
                        msg = e.Message.ToString()
                    };
                }
            }
            return result;
        }

        public dynamic Upd(APP_MENU data)
        {
            dynamic result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var Param = new DynamicParameters();
                    Param.Add("XID", data.ID);
                    Param.Add("XNAMEOPTION", data.NAMEOPTION);
                    Param.Add("XCONTROLLER", data.CONTROLLER);
                    Param.Add("XACTION", data.ACTION);
                    Param.Add("XIMAGECLASS", data.IMAGECLASS);
                    Param.Add("XSTATUS", data.STATUS);
                    Param.Add("XISPARENT", data.ISPARENT);
                    Param.Add("XPARENTID", data.PARENTID);
                    Param.Add("XSTS", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    Param.Add("XMSG", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("UPD_MENU", Param, null, null, commandType: CommandType.StoredProcedure);

                    result = new
                    {
                        sts = Param.Get<string>("XSTS"),
                        msg = Param.Get<string>("XMSG")
                    };
                }
                catch (Exception e)
                {
                    result = new
                    {
                        sts = "E",
                        msg = e.Message.ToString()
                    };
                }
            }
            return result;
        }
    }
}