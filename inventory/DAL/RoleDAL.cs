﻿using Dapper;
using inventory.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace inventory.DAL
{
    public class RoleDAL
    {
        public IEnumerable<APP_ROLE> GetDataRole()
        {
            IEnumerable<APP_ROLE> AllData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string query = "SELECT ROLE_ID,ROLE_NAME,REC_STAT FROM APP_ROLE ORDER BY ROLE_ID";
                    AllData = connection.Query<APP_ROLE>(query).ToList();
                }
                catch (Exception)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public IEnumerable<MENU> GetTreeMenu()
        {
            IEnumerable<MENU> AllData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string query = "SELECT ID,PARENTID,NAMEOPTION NAME, 'true' checked FROM APP_MENU ORDER BY PARENTID, ID";
                    AllData = connection.Query<MENU>(query).ToList();
                }
                catch (Exception)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public List<APP_MENU> GenerateMenu()
        {
            List<APP_MENU> AllData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string query = "SELECT ID,NAMEOPTION,CONTROLLER,ACTION,IMAGECLASS,STATUS,ISPARENT,PARENTID,ORDER_BY FROM APP_MENU ORDER BY PARENTID, ID";
                    AllData = connection.Query<APP_MENU>(query).ToList();
                }
                catch (Exception)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public dynamic Add(APP_ROLE data)
        {
            dynamic result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var Param = new DynamicParameters();
                    Param.Add("XROLE_NAME", data.ROLE_NAME);
                    Param.Add("XREC_STAT", data.REC_STAT);
                    Param.Add("XSTS", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    Param.Add("XMSG", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("INS_ROLE", Param, null, null, commandType: CommandType.StoredProcedure);

                    result = new
                    {
                        sts = Param.Get<string>("XSTS"),
                        msg = Param.Get<string>("XMSG")
                    };
                }
                catch (Exception e)
                {
                    result = new
                    {
                        sts = "E",
                        msg = e.Message.ToString()
                    };
                }
            }
            return result;
        }

        public dynamic Del(APP_ROLE data)
        {
            dynamic result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var Param = new DynamicParameters();
                    Param.Add("XROLE_ID", data.ROLE_ID);
                    Param.Add("XSTS", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    Param.Add("XMSG", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("DEL_ROLE", Param, null, null, commandType: CommandType.StoredProcedure);

                    result = new
                    {
                        sts = Param.Get<string>("XSTS"),
                        msg = Param.Get<string>("XMSG")
                    };
                }
                catch (Exception e)
                {
                    result = new
                    {
                        sts = "E",
                        msg = e.Message.ToString()
                    };
                }
            }
            return result;
        }

        public dynamic Upd(APP_ROLE data)
        {
            dynamic result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var Param = new DynamicParameters();
                    Param.Add("XROLE_ID", data.ROLE_ID);
                    Param.Add("XROLE_NAME", data.ROLE_NAME);
                    Param.Add("XREC_STAT", data.REC_STAT);
                    Param.Add("XSTS", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    Param.Add("XMSG", string.Empty, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("UPD_ROLE", Param, null, null, commandType: CommandType.StoredProcedure);

                    result = new
                    {
                        sts = Param.Get<string>("XSTS"),
                        msg = Param.Get<string>("XMSG")
                    };
                }
                catch (Exception e)
                {
                    result = new
                    {
                        sts = "E",
                        msg = e.Message.ToString()
                    };
                }
            }
            return result;
        }
    }
}