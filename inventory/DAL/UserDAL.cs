﻿using Dapper;
using inventory.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace inventory.DAL
{
    public class UserDAL
    {
        public List<APP_USER_LOKAL> GetDataUser(int role_id)
        {
            string query = "";
            List<APP_USER_LOKAL> AllData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    query = @"SELECT * FROM APP_USER WHERE ROLE_ID >= :ROLE_ID";
                    AllData = connection.Query<APP_USER_LOKAL>(query,new {
                        ROLE_ID = role_id
                    }).ToList();
                }
                catch (Exception)
                {
                    AllData = null;
                }
            }
            return AllData;
        }
    }
}