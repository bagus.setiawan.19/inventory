﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Models
{
    public class OutputModel
    {
        public string sts { get; set; }
        public string msg { get; set; }
    }

    public class Select2Model
    {
        public string KODE { get; set; }
        public string NAMA { get; set; }
    }

    class Node
    {
        [JsonProperty(PropertyName = "nodes")] public List<Node> Children = new List<Node>();

        public bool ShouldSerializeChildren()
        {
            return (Children.Count > 0);
        }

        public Node Parent { get; set; }
        public int Id { get; set; }
        public int? ParentId { get; set; }
        
        [JsonProperty(PropertyName = "text")]
        public string Name { get; set; }

        public string state { get; set; }

        [JsonProperty(PropertyName = "checked")]
        public string Checked { get; set; }

    }

    class State
    {
        [JsonProperty(PropertyName = "state")] public List<State> state = new List<State>();

        [JsonProperty(PropertyName = "checked")]
        public string Checked { get; set; }
    }
}