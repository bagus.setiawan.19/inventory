﻿var Role = function () {

    $('#btn-update').hide();

    var basicSelect2 = function () {
        $('.select2').select2();
    }

    var btnlihat = function () {
        $('#btn-lihat').click(function () {
            initTableRole();
            btnedit();
            hapus();
            $('#modalRole').modal('show');
        });
    }

    var btnsimpan = function () {
        $('#btn-simpan').click(function () {
            simpan();
        });
    }

    var btnupdate = function () {
        $('#btn-update').click(function () {
            update();
        });
    }


    var btnbatal = function () {
        $('#btn-batal').click(function () {
            clear();
        });
    }

    var simpan = function () {
        //var xId = $('#inputId').val();
        var xnameOption = $('#inputNameOption').val();
        var xcontroller = $('#inputController').val();
        var xaction = $('#inputAction').val();
        var ximageclass = $('#inputImageClass').val();
        var xstatus = $('#inputStatus').val();
        var xisparent = $('#inputIsParent').val();
        var xparentid = $('#inputParentId').val();

        if (xnameOption && xcontroller && xaction && ximageclass && xstatus && xisparent && xparentid) {
            var param = {
                nameOption: xnameOption,
                controller: xcontroller,
                action: xaction,
                imageclass: ximageclass,
                status: xstatus,
                isparent: xisparent,
                parentid: xparentid
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Menu/AddData",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.sts === 'E') {
                    swal('Error!', data.msg, 'error');
                } else {
                    clear();
                    swal('Success', data.msg, 'success');
                }
            });
        } else {
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    }

    var update = function () {
        var xRole_Id = $('#inputRoleId').val();
        var xRole_Name = $('#inputRoleName').val();
        var xRec_Stat = $('#inputRecStat').val();

        if (xRole_Id && xRole_Name && xRec_Stat) {
            var param = {
                Role_id: xRole_Id,
                Role_Name: xRole_Name,
                Rec_Stat: xRec_Stat
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Role/UpdData",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.sts === 'E') {
                    swal('Error!', data.msg, 'error');
                } else {
                    clear();
                    swal('Success', data.msg, 'success');
                }
            });
        } else {
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    }

    var hapus = function () {
        $('#tabelRole').on('click', '#btnDelete', function (e) {
            swal({
                title: "Apakah anda yakin ingin menghapus data ini ?",
                text: "",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "Tidak, batalkan !",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Ya, lanjutkan !",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(isConfirm => {
                if (isConfirm) {
                    var xTable = $('#tabelMenu').dataTable();
                    var nRow = $(this).parents('tr')[0];
                    var x = xTable.fnGetData(nRow);
                    var data = {
                        ID: x["ID"]
                    }
                    var req = $.ajax({
                        contentType: "application/json",
                        method: "POST",
                        data: JSON.stringify(data),
                        url: "/Role/DelData"
                    });
                    req.done(function (data) {
                        if (data.sts === 'S') {
                            swal('Success', data.msg, 'success');
                        } else {
                            swal('Error!', data.msg, 'error');
                        }
                    })
                        .fail(function () {
                            swal('Fail!', data.msg, 'error');
                        });
                } else {
                    swal("Cancelled", "Data batal dihapus :)", "error");
                }
            });
            initTableMenu();
        });

    }

    var clear = function () {
        $('#inputRoleId').val('');
        $('#inputRoleName').val('');
        $('#inputRecStat').val('');
        
        $('#btn-update').hide();
        $('#btn-simpan').show();
    }

    var initTableRole = function () {
        $('#tabelRole').DataTable({
            "ajax": {
                "url": "/Role/getDataRole",
                "type": "GET",
                "datatype": "json",
                "data": function (data) {
                },
            },
            "columns": [
                {
                    "data": "ROLE_ID"
                },
                {
                    "data": "ROLE_NAME"
                },
                {
                    "data": "REC_STAT"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<button id="btnEdit" type="button" class="btn btn-min-width mr-1 mb-1 box-shadow-4 btn-sm btn-warning">Edit</button>&nbsp;<button id="btnDelete" type="button" class="btn btn-min-width mr-1 mb-1 box-shadow-4 btn-sm btn-danger">Delete</button>';
                        return aksi;
                    }
                }
            ],
            //"lengthMenu": [[2, -1], [2, "All"]],
            //"sDom": "<'table-responsive't><'row'<p i>>",
            //"paging": false,
            //"searching": false,
            //"info": false,
            "processing": true,
            "destroy": true
        });
    }

    var btnedit = function () {
        $('body').on('click', 'tr #btnEdit', function () {
            var table = $('#tabelRole').dataTable();
            var nRow = $(this).parents('tr')[0];
            var data = table.fnGetData(nRow);
            $('#inputRoleId').val(data["ROLE_ID"]);
            $('#inputRoleName').val(data["ROLE_NAME"]);
            $('#inputRecStat').val(data["REC_STAT"]);

            $('#modalRole').modal('hide');

            $('#btn-update').show();
            $('#btn-simpan').hide();

        });
    }

    var menuTree = function () {
        $.ajax({
            contentType: "application/json",
            method: "GET",
            data: JSON.stringify(),
            url: "/Role/getTreeMenu"
        }).done(function (data) {
            $('#treeview').treeview({
                data: data,
                showCheckbox: true,
                onNodeChecked: function (event, node) {
                    swal('Information', 'Id : ' + node.Id + '  ' + node.text + ' was checked.', 'info');
                },
                onNodeUnchecked: function (event, node) {
                    swal('Warning', 'Id : ' + node.Id + '  ' + node.text + ' was unchecked.', 'warning');
                }
            })
        })
    }

    
    return {
        init: function () {
            basicSelect2();
            btnlihat();
            btnsimpan();
            btnupdate();
            btnbatal();
            hapus();
            menuTree();
        }
    };
}();

$(document).ready(function () {
    Role.init();
});