﻿var Menu = function () {

    $('#btn-update').hide();

    var basicSelect2 = function () {
        $('.select2').select2();
    }

    var btnlihat = function () {
        $('#btn-lihat').click(function () {
            initTableMenu();
            btnedit();
            hapus();
            $('#xlarge').modal('show');
        });
    }

    var btnsimpan = function () {
        $('#btn-simpan').click(function () {
            simpan();
        });
    }

    var btnupdate = function () {
        $('#btn-update').click(function () {
            update();
        });
    }

    //var btndelete = function () {
    //    $('#btnDelete').click(function () {
    //        hapus();
    //    });
    //}

    var btnbatal = function () {
        $('#btn-batal').click(function () {
            clear();
        });
    }

    var simpan = function () {
        //var xId = $('#inputId').val();
        var xnameOption = $('#inputNameOption').val();
        var xcontroller = $('#inputController').val();
        var xaction = $('#inputAction').val();
        var ximageclass = $('#inputImageClass').val();
        var xstatus = $('#inputStatus').val();
        var xisparent = $('#inputIsParent').val();
        var xparentid = $('#inputParentId').val();

        if (xnameOption && xcontroller && xaction && ximageclass && xstatus && xisparent && xparentid) {
            var param = {
                nameOption: xnameOption,
                controller: xcontroller,
                action: xaction,
                imageclass: ximageclass,
                status: xstatus,
                isparent: xisparent,
                parentid: xparentid
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Menu/AddData",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.sts === 'E') {
                    swal('Error!', data.msg, 'error');
                } else {
                    clear();
                    swal('Success', data.msg, 'success');
                }
            });
        } else {
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    }

    var update = function () {
        var xId = $('#inputId').val();
        var xnameOption = $('#inputNameOption').val();
        var xcontroller = $('#inputController').val();
        var xaction = $('#inputAction').val();
        var ximageclass = $('#inputImageClass').val();
        var xstatus = $('#inputStatus').val();
        var xisparent = $('#inputIsParent').val();
        var xparentid = $('#inputParentId').val();

        if (xId && xnameOption && xcontroller && xaction && ximageclass && xstatus && xisparent && xparentid) {
            var param = {
                id: xId,
                nameOption: xnameOption,
                controller: xcontroller,
                action: xaction,
                imageclass: ximageclass,
                status: xstatus,
                isparent: xisparent,
                parentid: xparentid
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Menu/UpdData",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.sts === 'E') {
                    swal('Error!', data.msg, 'error');
                } else {
                    clear();
                    swal('Success', data.msg, 'success');
                }
            });
        } else {
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    }

    var hapus = function () {
        $('#tabelMenu').on('click', '#btnDelete', function (e) {
            swal({
                title: "Apakah anda yakin ingin menghapus data ini ?",
                text: "",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "Tidak, batalkan !",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Ya, lanjutkan !",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(isConfirm => {
                if (isConfirm) {
                    var xTable = $('#tabelMenu').dataTable();
                    var nRow = $(this).parents('tr')[0];
                    var x = xTable.fnGetData(nRow);
                    var data = {
                        ID: x["ID"]
                    }
                    var req = $.ajax({
                        contentType: "application/json",
                        method: "POST",
                        data: JSON.stringify(data),
                        url: "/Menu/DelData"
                    });
                    req.done(function (data) {
                        if (data.sts === 'S') {
                            swal('Success', data.msg, 'success');
                        } else {
                            swal('Error!', data.msg, 'error');
                        }
                    })
                        .fail(function () {
                            swal('Fail!', data.msg, 'error');
                        });
                } else {
                    swal("Cancelled", "Data batal dihapus :)", "error");
                }
                });
            initTableMenu();
        });
        
    }

    var clear = function () {
        $('#inputId').val('');
        $('#inputNameOption').val('');
        $('#inputController').val('');
        $('#inputAction').val('');
        $('#inputImageClass').val('');
        $('#inputStatus').val('');
        $('#inputIsParent').val('');
        $('#inputParentId').val('');

        $('#btn-update').hide();
        $('#btn-simpan').show();
    }

    var initTableMenu = function () {
        $('#tabelMenu').DataTable({
            "ajax": {
                "url": "/Menu/getDataMenu",
                "type": "GET",
                "datatype": "json",
                "data": function (data) {
                },
            },
            "columns": [
                {
                    "data": "ID"
                },
                {
                    "data": "NAMEOPTION"
                },
                {
                    "data": "CONTROLLER"
                },
                {
                    "data": "ACTION"
                },
                {
                    "data": "IMAGECLASS"
                },
                {
                    "data": "STATUS"
                },
                {
                    "data": "ISPARENT"
                },
                {
                    "data": "PARENTID"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<button id="btnEdit" type="button" class="btn btn-min-width mr-1 mb-1 box-shadow-4 btn-sm btn-warning">Edit</button>&nbsp;<button id="btnDelete" type="button" class="btn btn-min-width mr-1 mb-1 box-shadow-4 btn-sm btn-danger">Delete</button>';
                        return aksi;
                    }
                }
            ],
            //"lengthMenu": [[2, -1], [2, "All"]],
            //"sDom": "<'table-responsive't><'row'<p i>>",
            //"paging": false,
            //"searching": false,
            //"info": false,
            "processing": true,
            "destroy": true
        });
    }

    var btnedit = function () {
        $('body').on('click', 'tr #btnEdit', function () {
            var table = $('#tabelMenu').dataTable();
            var nRow = $(this).parents('tr')[0];
            var data = table.fnGetData(nRow);
            $('#inputId').val(data["ID"]);
            $('#inputNameOption').val(data["NAMEOPTION"]);
            $('#inputController').val(data["CONTROLLER"]);
            $('#inputAction').val(data["ACTION"]);
            $('#inputImageClass').val(data["IMAGECLASS"]);
            $('#inputStatus').val(data["STATUS"]);
            $('#inputIsParent').val(data["ISPARENT"]);
            $('#inputParentId').val(data["PARENTID"]);

            $('#xlarge').modal('hide');

            $('#btn-update').show();
            $('#btn-simpan').hide();

        });
    }

    var select2ParentMenu = function () {
        $("#inputParentId").select2({
            placeholder: "Select parent menu",
            //theme: "classic",
            ajax: {
                url: "/Helper/GetParentMenu",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.NAMA,
                                id: item.KODE
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            //minimumInputLength: 1
            minimumInputLength: 0 // untuk menampilkan semua
        });
    }

    return {
        init: function () {
            basicSelect2();
            btnlihat();
            btnsimpan();
            btnupdate();
            btnbatal();
            hapus();
            select2ParentMenu();
        }
    };
}();

$(document).ready(function () {
    Menu.init();
});