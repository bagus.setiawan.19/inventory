﻿using inventory.DAL;
using inventory.Entities;
using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using inventory.Models;
using Microsoft.Ajax.Utilities;

namespace inventory.Controllers
{
    public class RoleController : Controller
    {
        private RoleDAL dal = new RoleDAL();

        // GET: Role
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getDataRole()
        {
            var result = dal.GetDataRole();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTreeMenu()
        {
            var result = dal.GetTreeMenu();
            var tree = RawCollectionToTree(result);
            string str = JsonConvert.SerializeObject(tree, Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                });
            return Json(str, JsonRequestBehavior.AllowGet);
        }

        private object RawCollectionToTree(IEnumerable<MENU> result)
        {
            var treeDictionary = new Dictionary<int?, Node>();
            result.ForEach(x => treeDictionary.Add(x.Id, new Node { Id = x.Id, ParentId = x.ParentId, Name = x.Name}));
            result.ForEach(x => treeDictionary.Add(x.Id, new Node { Id = x.Id, ParentId = x.ParentId, Name = x.Name, state = x.Checked}));
            foreach (var item in treeDictionary.Values)
            {
                if (item.ParentId != null)
                {
                    Node proposedParent;
                    if (treeDictionary.TryGetValue(item.ParentId,out proposedParent))
                    {
                        item.Parent = proposedParent;
                        proposedParent.Children.Add(item);
                    }
                }
            }
            return treeDictionary.Values.Where(x => x.Parent == null);
        }

        //public ActionResult generateMenu()
        //{
        //    var result = dal.GetTreeMenu();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult getTreeData(int mainItem)
        //{
        //    TREE_DATA[] returnObject = new TREE_DATA[3];

        //    TREE_DATA rootNode1 = new TREE_DATA();
        //    rootNode1.text = "Username 1";
        //    rootNode1.tags = new string[1];
        //    rootNode1.tags[0] = "1";
        //    returnObject[0] = rootNode1;

        //    rootNode1.nodes = new TREE_DATA[2];

        //    TREE_DATA childNode = new TREE_DATA();
        //    childNode.text = "First name1";
        //    childNode.tags = new string[1];
        //    childNode.tags[0] = "11";
        //    rootNode1.nodes[0] = childNode;

        //    childNode = new TREE_DATA();
        //    childNode.text = "Last name1";
        //    childNode.tags = new string[1];
        //    childNode.tags[0] = "12";
        //    rootNode1.nodes[1] = childNode;

        //    TREE_DATA rootNode2 = new TREE_DATA();
        //    rootNode2.text = "Username 2";
        //    rootNode2.tags = new string[1];
        //    rootNode2.tags[0] = "2";
        //    returnObject[1] = rootNode2;

        //    TREE_DATA rootNode3 = new TREE_DATA();
        //    rootNode3.text = "Username 3";
        //    rootNode3.tags = new string[1];
        //    rootNode3.tags[0] = "3";
        //    returnObject[2] = rootNode3;

        //    return Json(returnObject);
        //}


        public JsonResult AddData(APP_ROLE data)
        {
            dynamic message = null;
            try
            {
                message = dal.Add(data);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdData(APP_ROLE data)
        {
            dynamic message = null;
            try
            {
                message = dal.Upd(data);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DelData(APP_ROLE data)
        {
            dynamic message = null;
            try
            {
                message = dal.Del(data);
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}