﻿using inventory.DAL;
using inventory.Entities;
using inventory.Models;
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace inventory.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                var claimsIdentity = User.Identity as System.Security.Claims.ClaimsIdentity;
                return RedirectToAction(claimsIdentity.FindFirst("URL_HOME").Value, "Home");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
        }

        public JsonResult AuthLogin(LoginModel model)
        {
            string xsts = "";
            string xmsg = "";
            string url = "";
            try
            {
                AuthDAL dal = new AuthDAL();
                string vNAMA_TERMINAL = "";
                APP_USER userInformation = dal.GetUserInformation(model.NIPP, model.Password);
                //List<VW_MENU_ACTIVITIES> userActivities = dal.GetUserActivities(userInformation.USER_ROLE_ID);

                if (userInformation.USER_ROLE_ID != "E")
                {
                    vNAMA_TERMINAL = userInformation.NAMA_TERMINAL;

                    //if (userInformation.JNS_DEPO == "MTY")
                    //{
                    //    url = "IndexEmpty";
                    //    xmsg = "/Home/IndexEmpty";
                    //}
                    //else if (userInformation.JNS_DEPO == "PLP")
                    //{
                    //    url = "IndexPLP";
                    //    xmsg = "/Home/IndexPLP";
                    //}
                    //else if (userInformation.JNS_DEPO == "PLB")
                    //{
                    //    url = "IndexPLB";
                    //    xmsg = "/Home/IndexPLB";
                    //}
                    //else
                    //{
                    //    url = "IndexEmpty";
                    //    xmsg = "/Home/IndexEmpty";
                    //}

                    url = "Index";
                    xmsg = "/Home";

                    var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, userInformation.USER_NAME),
                        new Claim("USER_ID", userInformation.ID),
                        new Claim("KD_CABANG", userInformation.KD_CABANG),
                        new Claim("KD_TERMINAL", userInformation.KD_TERMINAL),
                        new Claim("KD_CABANG_ORIG", userInformation.KD_CABANG),
                        new Claim("KD_TERMINAL_ORIG", userInformation.KD_TERMINAL),
                        new Claim("USER_ROLE", userInformation.USER_ROLE_ID),
                        new Claim("USER_ROLE_NAME", userInformation.USER_ROLE_NAME),
                        new Claim("NAMA_CABANG", userInformation.NAMA_CABANG),
                        new Claim("NAMA_TERMINAL", vNAMA_TERMINAL),
                        new Claim("URL_HOME", url),
                        new Claim("LO_ID", userInformation.LO_ID),
                        new Claim("REGIONAL", userInformation.REGIONAL)
                    }, "ApplicationCookie");

                    //if (userActivities.Count > 0)
                    //{
                    //    foreach (var item in userActivities)
                    //    {
                    //        if (item.PERMISSION_ADD.Equals("1"))
                    //            identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Add"));
                    //        if (item.PERMISSION_DEL.Equals("1"))
                    //            identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Delete"));
                    //        if (item.PERMISSION_UPD.Equals("1"))
                    //            identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Edit"));
                    //        if (item.PERMISSION_VIW.Equals("1"))
                    //            identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".View"));
                    //    }
                    //}

                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;
                    authManager.SignIn(identity);
                }
                xsts = userInformation.KD_CABANG;
                if (xsts == "E")
                {
                    xmsg = userInformation.NAMA_CABANG;
                }
            }
            catch (Exception ex)
            {
                xsts = "E";
                xmsg = ex.Message;
            }
            OutputModel ret = new OutputModel();
            ret.sts = xsts;
            ret.msg = xmsg;
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                try
                {
                    var claimsIdentity = User.Identity as System.Security.Claims.ClaimsIdentity;
                    return Url.Action(claimsIdentity.FindFirst("URL_HOME").Value, "Home");
                }
                catch (Exception e)
                {
                    return Url.Action("Index", "Home");

                }
            }

            return returnUrl;
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }



        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}