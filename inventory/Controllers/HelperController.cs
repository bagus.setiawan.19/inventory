﻿using inventory.DAL;
using inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace inventory.Controllers
{
    public class HelperController : Controller
    {
        public JsonResult GetParentMenu()
        {
            IEnumerable<Select2Model> result = null;
            HelperDAL dal = new HelperDAL();
            result = dal.getSelect2ParentMenu();
            //var items = result;
            //return Json(items, JsonRequestBehavior.AllowGet);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }
    }
}