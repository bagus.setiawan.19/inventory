﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Entities
{
    public class APP_ROLE
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string REC_STAT { get; set; }
    }
}