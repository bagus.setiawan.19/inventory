﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Entities
{
    public class APP_MENU
    {
        public string ID { get; set; }
        public string NAMEOPTION { get; set; }
        public string CONTROLLER { get; set; }
        public string ACTION { get; set; }
        public string IMAGECLASS { get; set; }
        public string STATUS { get; set; }
        public string ISPARENT { get; set; }
        public string PARENTID { get; set; }
        public string ORDER_BY { get; set; }
    }
}