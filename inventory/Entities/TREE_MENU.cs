﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.Entities
{
    //[Serializable]
    //public class TREE_DATA
    //{
    //    public string text { get; set; }
    //    public string[] tags { get; set; }
    //    public TREE_DATA[] nodes { get; set; }
    //}

    public class MENU
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string Checked { get; set; }
        //public MENU[] nodes { get; set; }
    }

    //public class TREE_MENU
    //{
    //    public int Id { get; set; }
    //    public int ParentId { get; set; }
    //    public string text { get; set; }
    //    public List<TREE_MENU_D> nodes { get; set; }
    //}

    //public class TREE_MENU_D
    //{
    //    public int Id { get; set; }
    //    public int ParentId { get; set; }
    //    public string text { get; set; }
    //}
}